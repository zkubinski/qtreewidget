#ifndef SUBSUBPAGE0_H
#define SUBSUBPAGE0_H

#include <QWidget>
#include <QLabel>

class Subsubpage0 : public QWidget
{
    Q_OBJECT
public:
    explicit Subsubpage0(QWidget *parent = nullptr);

private:
    QLabel *label;

signals:

};

#endif // SUBSUBPAGE0_H
