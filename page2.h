#ifndef PAGE2_H
#define PAGE2_H

#include <QWidget>
#include <QLabel>

class Page2 : public QWidget
{
    Q_OBJECT
public:
    explicit Page2(QWidget *parent = nullptr);

private:
    QLabel *label;

signals:

};

#endif // PAGE2_H
