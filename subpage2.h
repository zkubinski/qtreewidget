#ifndef SUBPAGE2_H
#define SUBPAGE2_H

#include <QWidget>
#include <QLabel>

class Subpage2 : public QWidget
{
    Q_OBJECT
public:
    explicit Subpage2(QWidget *parent = nullptr);

private:
    QLabel *label;

signals:

};

#endif // SUBPAGE2_H
