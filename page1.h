#ifndef PAGE1_H
#define PAGE1_H

#include <QWidget>
#include <QLabel>

class Page1 : public QWidget
{
    Q_OBJECT
public:
    explicit Page1(QWidget *parent = nullptr);

private:
    QLabel *label;

signals:

};

#endif // PAGE1_H
