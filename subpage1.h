#ifndef SUBPAGE1_H
#define SUBPAGE1_H

#include <QWidget>
#include <QLabel>

class Subpage1 : public QWidget
{
    Q_OBJECT
public:
    explicit Subpage1(QWidget *parent = nullptr);

private:
    QLabel *label;

signals:

};

#endif // SUBPAGE1_H
