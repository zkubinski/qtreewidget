#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QStackedWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "page0.h"
#include "subpage0.h"
#include "subsubpage0.h"
#include "subpage1.h"
#include "subpage2.h"
#include "page1.h"
#include "page2.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QWidget *mainWidget;
    QVBoxLayout *mainLayout;
    QHBoxLayout *layoutWidget, *layoutButton;
    QTreeWidget *treeWidget;
    QTreeWidgetItem *treeWidgetItm1, *child1WidgetItm1, *child1child1WidgetItm1, *child2WidgetItm1, *child3WidgetItm1,
    *treeWidgetItm2, *treeWidgetItm3;
    QStackedWidget *rootStackedWidget, *childStackedWidget;
    QPushButton *pbExit;

    Page0 *page0;
    Subpage0 *subpage0;
    Subsubpage0 *subsubpage0;
    Subpage1 *subpage1;
    Subpage2 *subpage2;
    Page1 *page1;
    Page2 *page2;

    int numberPage;

public slots:
    void ShowItemPage(QTreeWidgetItem *item, int column);

signals:
    void ShowNumberPage(int);
};
#endif // MAINWINDOW_H
