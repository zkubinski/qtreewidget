#include "mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
{
    this->resize(800,600);

    mainWidget = new QWidget(this);
    mainLayout = new QVBoxLayout(mainWidget);

    layoutWidget = new QHBoxLayout();

    QStringList labels;
    labels << "label1";

    treeWidget = new QTreeWidget();
    treeWidget->setFrameStyle(QFrame::Box | QFrame::Plain);
    treeWidget->setHeaderLabels(labels);

    treeWidgetItm1 = new QTreeWidgetItem(treeWidget, 0);
    treeWidgetItm1->setText(0,"test1");

    child1WidgetItm1 = new QTreeWidgetItem(treeWidgetItm1, 0);
    child1WidgetItm1->setText(0,"child1");

    child1child1WidgetItm1 = new QTreeWidgetItem(child1WidgetItm1, 2);
    child1child1WidgetItm1->setText(0, "child child 0");

    child2WidgetItm1 = new QTreeWidgetItem(treeWidgetItm1, 1);
    child2WidgetItm1->setText(0,"child2");

    child3WidgetItm1 = new QTreeWidgetItem(treeWidgetItm1, 2);
    child3WidgetItm1->setText(0, "child3");

    treeWidgetItm1->addChild(child1WidgetItm1);
    child1WidgetItm1->addChild(child1child1WidgetItm1);

    treeWidgetItm1->addChild(child2WidgetItm1);
    treeWidgetItm1->addChild(child3WidgetItm1);

    treeWidgetItm2 = new QTreeWidgetItem(treeWidget, 1);
    treeWidgetItm2->setText(0,"test2");

    treeWidgetItm3 = new QTreeWidgetItem(treeWidget, 2);
    treeWidgetItm3->setText(0,"test3");

    QList <QTreeWidgetItem*> item;
    item.append(treeWidgetItm1);
    item.append(treeWidgetItm2);

    treeWidget->addTopLevelItems(item);

    page0 = new Page0();
    subpage0 = new Subpage0();
    subsubpage0 = new Subsubpage0();
    subpage1 = new Subpage1();
    subpage2 = new Subpage2();
    page1 = new Page1();
    page2 = new Page2();

    rootStackedWidget = new QStackedWidget();
    rootStackedWidget->setFrameStyle(QFrame::Box | QFrame::Plain);
    rootStackedWidget->addWidget(page0);
    rootStackedWidget->addWidget(subpage0);
    rootStackedWidget->addWidget(subsubpage0);
    rootStackedWidget->addWidget(subpage1);
    rootStackedWidget->addWidget(subpage2);
    rootStackedWidget->addWidget(page1);
    rootStackedWidget->addWidget(page2);

    layoutWidget->addWidget(treeWidget);
    layoutWidget->addWidget(rootStackedWidget);

    layoutButton = new QHBoxLayout();
    pbExit = new QPushButton();
    pbExit->setText(QString(tr("Zamknij")));

    layoutButton->addWidget(pbExit);

    mainLayout->addLayout(layoutWidget);
    mainLayout->addLayout(layoutButton);

    this->setCentralWidget(mainWidget);

    QObject::connect(pbExit, &QPushButton::clicked, this, &QMainWindow::close);
    QObject::connect(treeWidget, &QTreeWidget::itemClicked, this, &MainWindow::ShowItemPage);
    QObject::connect(this, &MainWindow::ShowNumberPage, rootStackedWidget, &QStackedWidget::setCurrentIndex);
}

MainWindow::~MainWindow()
{
}

void MainWindow::ShowItemPage(QTreeWidgetItem *item, int column)
{
    (void) column;

//    QTreeWidgetItem *parent;

//    QList<QTreeWidgetItem*> selected;
//    selected = treeWidget->selectedItems();

//    if(treeWidget->parent()){
//        index = treeWidget->indexOfTopLevelItem(item);
//        index = idx.row();
//        qDebug()<< "p" << index;
//    }
//    if(selected.at(0)->parent()){
//        index = treeWidgetItm1->indexOfChild(item);
//        index = idx.row();
//        qDebug()<< "ch" << index;
//    }

    if(treeWidget->indexOfTopLevelItem(item) == 0){
        emit ShowNumberPage(0);
    }
        if(treeWidgetItm1->indexOfChild(item) == 0){
            emit ShowNumberPage(1);
        }
            if(child1WidgetItm1->indexOfChild(item) == 0){
                emit ShowNumberPage(2);
            }
        if(treeWidgetItm1->indexOfChild(item) == 1){
            emit ShowNumberPage(3);
        }
        if(treeWidgetItm1->indexOfChild(item) == 2){
            emit ShowNumberPage(4);
        }

    if(treeWidget->indexOfTopLevelItem(item) == 1){
        emit ShowNumberPage(5);
    }
    if(treeWidget->indexOfTopLevelItem(item) == 2){
        emit ShowNumberPage(6);
    }

//    item->setData(column, Qt::UserRole, rootStackedWidget->addWidget(page0));

//    if(treeWidgetItm1){
//        index = child1WidgetItm1->indexOfChild(item);
//    }

//    qDebug()<< index;
}

