#include "page0.h"

Page0::Page0(QWidget *parent) : QWidget(parent)
{
    mainWidget = new QWidget(this);
    mainLayout = new QVBoxLayout(mainWidget/*this*/);

    layout = new QHBoxLayout();

    labelText = new QLabel(this);
    labelText->setText("Wpisz tekst");

    labelShowText = new QLabel(this);

    editText = new QLineEdit(this);

    pbClear = new QPushButton(this);
    pbClear->setText(QString(tr("Wyczyść")));

    layout->addWidget(labelText);
    layout->addWidget(editText);

    mainLayout->addLayout(layout);
    mainLayout->addWidget(labelShowText);
    mainLayout->addWidget(pbClear);

    mainWidget->setLayout(mainLayout);

    QObject::connect(editText, &QLineEdit::textChanged, labelShowText, &QLabel::setText);
    QObject::connect(pbClear, &QPushButton::clicked, editText, &QLineEdit::clear);
}
