#ifndef SUBPAGE0_H
#define SUBPAGE0_H

#include <QWidget>
#include <QLabel>

class Subpage0 : public QWidget
{
    Q_OBJECT
public:
    explicit Subpage0(QWidget *parent = nullptr);

private:
    QLabel *label;

signals:

};

#endif // SUBPAGE0_H
