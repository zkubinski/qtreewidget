#ifndef PAGE0_H
#define PAGE0_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class Page0 : public QWidget
{
    Q_OBJECT
public:
    explicit Page0(QWidget *parent = nullptr);

private:
    QWidget *mainWidget;
    QVBoxLayout *mainLayout;
    QHBoxLayout *layout;
    QLabel *labelText, *labelShowText;
    QLineEdit *editText;
    QPushButton *pbClear;

signals:

};

#endif // PAGE0_H
